//
//  User.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 17/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

//MARK: - User
public struct User: Glossy {
    
    public var deviceId : String?
    public var email : String?
    public var name : String?
    public var picture : String?
    public var pro : Bool?
    public var productIds : [String]?
    public var uberId : String?
    
    
    public init(){
        
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        deviceId = "deviceId" <~~ json
        email = "email" <~~ json
        name = "name" <~~ json
        picture = "picture" <~~ json
        pro = "pro" <~~ json
        productIds = "product_ids" <~~ json
        uberId = "uberId" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "deviceId" ~~> deviceId,
            "email" ~~> email,
            "name" ~~> name,
            "picture" ~~> picture,
            "pro" ~~> pro,
            "product_ids" ~~> productIds,
            "uberId" ~~> uberId,
            ])
    }
    
}
