//
//  SettingsViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 15/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import Instabug
import UberRides

class SettingsViewController : UITableViewController {
    
    @IBOutlet weak var bugCell: UITableViewCell!
    @IBOutlet weak var subscriptionCell: UITableViewCell!
    
    override func viewDidLoad() {
        setupTaps()
    }
    
    func setupTaps(){
        let bugGesture = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.showInstabug))
        self.bugCell.addGestureRecognizer(bugGesture)
        
        let subGesture = UITapGestureRecognizer(target: self, action: #selector(SettingsViewController.manageSubscriptions))
        self.subscriptionCell.addGestureRecognizer(subGesture)
    }
    
    
    func manageSubscriptions(){
        UIApplication.shared.open(NSURL(string:"https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/manageSubscriptions") as! URL, options: [:], completionHandler: nil)
    }
    
    func showInstabug(){
        Instabug.invoke()
    }
    
    func logout(){

    }
    
}
