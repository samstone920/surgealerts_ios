//
//  UIColor + Utils.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit


extension UIColor {
    
    class func saOrange() -> UIColor{
        return UIColor(red: 0.965, green: 0.563, blue: 0.176, alpha: 1.00)
    }
    
}
