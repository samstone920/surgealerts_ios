//
//  ChartsViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip
import Charts

class ChartsViewController : UIViewController, IndicatorInfoProvider {
    
    var itemInfo = IndicatorInfo(title: "Charts")
    
    @IBOutlet weak var barChart: HorizontalBarChartView!
    
    init() {
        super.init(nibName: "ChartsViewController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        barChart.leftAxis.drawGridLinesEnabled = false
        barChart.rightAxis.drawGridLinesEnabled = false
        barChart.xAxis.drawGridLinesEnabled = false
        barChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
        updateChartWithData()
    }
    
    func updateChartWithData() {
        var dataEntries: [BarChartDataEntry] = []
        let visitorCounts = 10
        for i in 0..<visitorCounts {
            let dataEntry = BarChartDataEntry(x: Double(i), y: Double(arc4random_uniform(20) + 1))
            dataEntries.append(dataEntry)
        }
        let chartDataSet = BarChartDataSet(values: dataEntries, label: "Surge Count")
        chartDataSet.setColor(UIColor.saOrange())
        let chartData = BarChartData(dataSet: chartDataSet)
        self.barChart.data = chartData
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }
    
    
}
