//
//  SliderCell.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 14/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class SliderCell : UITableViewCell {
    
    @IBOutlet weak var surgeSlider: UISlider!
    @IBOutlet weak var surgeValue: UILabel!
    
    private var delegate : sliderDelegate?

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        update(self)
    }

    @IBAction func update(_ sender: Any) {
        surgeValue.text = String(roundValue(value: surgeSlider.value))
        self.delegate?.sliderValueDidChange(value: roundValue(value: surgeSlider.value))
    }
    
    func roundValue(value : Float) -> Float {
        return roundf(value / 0.1) * 0.1;
    }
    
    func refresh(delegate : sliderDelegate){
        self.delegate = delegate
    }
}
