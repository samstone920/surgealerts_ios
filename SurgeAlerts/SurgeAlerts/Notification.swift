//
//  Notification.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 16/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

//MARK: - RootClass
public struct Notification: Glossy {
    
    public let id : String!
    public let date : String!
    public let locationId : String!
    public let multiplyer : Float!
    public let name : String!
    public let read : Bool!
    public let started : Bool!
    public let uberId : String!
    
    
    
    //MARK: Decodable
    public init?(json: JSON){
        id = "_id" <~~ json
        date = "date" <~~ json
        locationId = "locationId" <~~ json
        multiplyer = "multiplyer" <~~ json
        name = "name" <~~ json
        read = "read" <~~ json
        started = "started" <~~ json
        uberId = "uberId" <~~ json
    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "_id" ~~> id,
            "date" ~~> date,
            "locationId" ~~> locationId,
            "multiplyer" ~~> multiplyer,
            "name" ~~> name,
            "read" ~~> read,
            "started" ~~> started,
            "uberId" ~~> uberId,
            ])
    }
    
}
