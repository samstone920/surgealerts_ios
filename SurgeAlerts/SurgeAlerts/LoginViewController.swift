//
//  LoginViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 14/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import UberRides
import OneSignal

class LoginViewController : UIViewController, LoginButtonDelegate {
    
    var uberScopes: [RidesScope]!
    var uberLoginManager: LoginManager!
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)


    @IBOutlet weak var surgeImage: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let scopes: [RidesScope] = [.Profile]
        let loginManager = LoginManager(loginType: .native)
        let loginButton = LoginButton(frame: CGRect(x:surgeImage.frame.origin.x, y:surgeImage.frame.origin.y + surgeImage.frame.size.height + 10, width: surgeImage.frame.size.width, height: 50), scopes: scopes, loginManager: loginManager)
        loginButton.presentingViewController = self
        loginButton.delegate = self
        if TokenManager.fetchToken() != nil{
            getUser()
            activityIndicator.frame = CGRect(x: surgeImage.frame.origin.x, y: surgeImage.frame.origin.y + surgeImage.frame.size.height + 10, width: surgeImage.frame.size.width, height: 50)
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        } else {
            view.addSubview(loginButton)
        }
    }
    
    
    func loginButton(_ button: LoginButton, didCompleteLoginWithToken accessToken: AccessToken?, error: NSError?) {
        if let token = accessToken {
            let t = AccessToken(tokenString: token.tokenString!)
            if TokenManager.saveToken(t) {
                getUser()
            } else {
                let alert = UIAlertController(title: "Error", message: "An error has occcured, please try again later", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            let alert = UIAlertController(title: "Error", message: "An error has occcured, please try again later", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func loginButton(_ button: LoginButton, didLogoutWithSuccess success: Bool) {
        
    }
    
    func getUser() {
        let ridesClient = RidesClient()
        ridesClient.fetchUserProfile { (rider, response) in
            if response.error == nil {
                if let r = rider, let uuid = r.UUID {
                    UserService.getUserForId(id: uuid, completion: { (success, user) in
                        if success {
                            if let u = user {
                                UserDefaults.saveUser(user: u)
                                self.performSegue(withIdentifier: "login", sender: nil)
                            }
                        } else {
                            self.addUser(rider: r)
                        }
                    })
                } else {
                    let alert = UIAlertController(title: "Error", message: "An error has occcured, please try again later", preferredStyle: UIAlertControllerStyle.alert)
                    alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        }
    }
    
    func addUser(rider : UserProfile){
        var user = User.init()
        if let first = rider.firstName, let last = rider.lastName{
            user.name = first + " " + last
        }
        user.uberId = rider.UUID
        user.email = rider.email
        user.pro = false
        user.picture = rider.picturePath
        user.productIds = []
        OneSignal.idsAvailable({ (userId, pushToken) in
            if (pushToken != nil) {
                user.deviceId = userId
            }
            UserService.addUser(parameters: user.toJSON() as [String : AnyObject]?, completion: { (success) in
                UserDefaults.saveUser(user: user)
                self.performSegue(withIdentifier: "login", sender: nil)
            })
        })
        
       
    }
    
}
