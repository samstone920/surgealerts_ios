//
//  NSUserDefaults + Utisl.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 17/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

extension UserDefaults{
    
    public static func saveUser(user : User){
        let defaults = UserDefaults.standard
        defaults.setValue(user.toJSON(), forKey: "json")
    }
    
    public static func getLoggedInUser() -> User? {
        let defaults = UserDefaults.standard
        return User.init(json: defaults.value(forKey: "json") as! JSON)
    }

}
