//
//  ProductSelectViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 17/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import UberRides
import Material


class ProductSelectViewController :  UITableViewController {
    
    enum ProductType : String {
        case POOL = "POOL"
        case X = "uberX"
        case XL = "uberXL"
        case SELECT = "SELECT"
        case BLACK = "BLACK"
        case SUV = "SUV"
        case ASSIST = "ASSIST"
        case WAV = "WAV"
        case TAXI  = "TAXI"
        
        static let allValues = [POOL, X, XL, SELECT, BLACK, SUV, ASSIST, WAV, TAXI]
    }
    
    private var selectedProducts : [String] = []

    @IBOutlet var switches: [UISwitch]!

    override func viewDidLoad() {
        var i = 0
        for sw in switches {
            sw.tag = i
            i += 1
            sw.setOn(false, animated: false)
            sw.addTarget(self, action: #selector(self.collectionIsChanged(sw:)), for: UIControlEvents.valueChanged)
        }
    }
    
    func collectionIsChanged(sw: UISwitch) {
        if sw.isOn{
            selectedProducts.append(ProductType.allValues[sw.tag].rawValue)
        } else {
            for (index, product) in selectedProducts.enumerated() {
                if product == ProductType.allValues[sw.tag].rawValue{
                    selectedProducts.remove(at: index)
                    break
                }
            }
        }
        print(selectedProducts)
        
    }
    
    @IBAction func done(_ sender: Any) {
        let done = sender as! UIBarButtonItem
        done.isEnabled = false
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        activityIndicator.frame = CGRect(x: view.center.x, y: view.center.y, width: 50, height: 50)
        activityIndicator.startAnimating()
        
        if var user = UserDefaults.getLoggedInUser(), let id = user.uberId {
            user.productIds = selectedProducts
             UserService.updateUser(id: id, parameters: user.toJSON() as [String : AnyObject]?, completion: { (success) in
                if success {
                    UserDefaults.saveUser(user: user)
                    self.dismiss(animated: true, completion: nil)
                } else {
                    done.isEnabled = true
                }
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
             })
        }
    }
    
}

