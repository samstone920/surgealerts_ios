//
//  Constants.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation

struct K {
    struct Animation {
        static let animationLength = 0.5
    }
    
    struct Frames {
        static let FABSize = 62.0
    }
}
