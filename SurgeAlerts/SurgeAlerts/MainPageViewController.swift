//
//  MainPageViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip

class MainPageViewController : ButtonBarPagerTabStripViewController {
    
    override func viewDidLoad() {
        self.title = "Surge Alerts"
        settings.style.selectedBarBackgroundColor = UIColor.saOrange()
        settings.style.selectedBarHeight = 5
        settings.style.buttonBarItemsShouldFillAvailiableWidth = true
        settings.style.buttonBarItemTitleColor = UIColor.black
        settings.style.buttonBarBackgroundColor = UIColor.white
        settings.style.buttonBarItemBackgroundColor = UIColor.white
        super.viewDidLoad()
        
        if let user = UserDefaults.getLoggedInUser() {
            if (user.productIds?.count == 0) {
                self.performSegue(withIdentifier: "setup", sender: nil)
            }
        }
    }
    
    override func viewControllers(for pagerTabStripController: PagerTabStripViewController) -> [UIViewController] {
        return [MapViewController(), ChartsViewController()]
    }
}
