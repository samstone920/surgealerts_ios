//
//  MapViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import XLPagerTabStrip
import MapKit
import Material


class MapViewController : UIViewController, IndicatorInfoProvider, CLLocationManagerDelegate, MKMapViewDelegate, UIGestureRecognizerDelegate {
    
    var itemInfo = IndicatorInfo(title: "Maps")
    
    let currentLocation = FabButton(image: UIImage(named: "currentLocation"), tintColor: .black)
    
    @IBOutlet weak var mapView: MKMapView!
    let locationManager = CLLocationManager()
    var buttonVisible = true
    
    init(){
        super.init(nibName: "MapViewController", bundle: Bundle.main)
        setupFAB()
        setupMap()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mapView.delegate = self
        let gestureRecognizer = UIPanGestureRecognizer(target: nil, action: nil)
        gestureRecognizer.delegate = self
        self.mapView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getLocations()
    }
    
    func getLocations(){
        if let user = UserDefaults.getLoggedInUser(){
        LocationService.getLocationForUser(id: user.uberId!, completion: { (success, locations) in
            if success {
                if let l = locations {
                    for location in l {
                        self.addLocation(location: location)
                    }
                }
            }
            
        })
    
        }
    }
    
    func addLocation(location : Location) {
        if let lat = location.lat, let longitude = location.longField {
            let center = CLLocationCoordinate2D(latitude: lat, longitude: longitude)
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = center
            mapView.addAnnotation(dropPin)
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool{
        if !buttonVisible {
            AnimationHelper.slide(yDelta: 300, xDelta: 0, view: self.currentLocation)
            buttonVisible = true
        }
        
        return false
    }
    
    func setupFAB(){
        let help = FabButton(image: UIImage(named: "question") , tintColor: .white)
        help.pulseColor = .white
        help.backgroundColor = Color.saOrange()
        
        currentLocation.pulseColor = .gray
        currentLocation.backgroundColor = Color.white
        
        view.layout(help)
            .width(62)
            .height(62)
            .bottomRight(bottom: 24, right: 24)
        
        help.addTarget(self, action: #selector(showHelp), for: .touchUpInside)

        view.layout(currentLocation)
            .width(62)
            .height(62)
            .bottomLeft(bottom: 24, left: 24)
        
        currentLocation.addTarget(self, action: #selector(getCurrentLocation), for: .touchUpInside)

    }
    
    func showHelp(){
        let window = UIApplication.shared.keyWindow
        let help = HelpOverlayView(frame: (window!.frame))
        help.alpha = 0
        window?.addSubview(help)
        AnimationHelper.fadeIn(duration: 0.3, view: help)
    }
    
    func getCurrentLocation(){
        buttonVisible = false
        AnimationHelper.slide(yDelta: -300, xDelta: 0, view: self.currentLocation)
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last! as CLLocation
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
        
        self.mapView.setRegion(region, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        if CLLocationManager.locationServicesEnabled() {
            locationManager.stopUpdatingLocation()
        }
    }
    
    
    
    func setupMap(){
        let uilgr = UILongPressGestureRecognizer(target: self, action: #selector(addAnnotation))
        uilgr.minimumPressDuration = 2.0
        mapView.addGestureRecognizer(uilgr)
    }
   
    func addAnnotation(gestureRecognizer:UIGestureRecognizer){
        let touchPoint = gestureRecognizer.location(in: mapView)
        let newCoordinates = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        let location = CLLocation(latitude: newCoordinates.latitude, longitude: newCoordinates.longitude)
        
        let VC1 = AddMarkerViewController.init(location: location)
        let navController = UINavigationController(rootViewController: VC1)
        self.present(navController, animated: true, completion: nil)
    }
    
    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

    
}
