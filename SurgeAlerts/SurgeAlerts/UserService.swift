//
//  UserService.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 17/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

class UserService : BaseService{
    
    static func getUserForId(id: String, completion: @escaping (Bool, User?) -> Void) {
        let url = "users/\(id)"
        getRequest(url: url, completion: {(result: JSON?) in
            if let json = result {
                completion(true, User.init(json: json))
            } else {
                completion(false, nil)
            }
        })
    }
    
    static func addUser(parameters : [String: AnyObject]? = nil, completion: @escaping (Bool) -> Void) {
        let url = "users"
        postRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
            completion(true)
        })
    }
    
    static func updateUser(id: String, parameters : [String: AnyObject]? = nil, completion: @escaping (Bool) -> Void) {
        let url = "users/\(id)"
        putRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
            completion(true)
        })
    }
    
}
