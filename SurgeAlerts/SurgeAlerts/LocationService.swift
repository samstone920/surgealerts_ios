//
//  LocationService.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 16/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire
import UIKit

class LocationService : BaseService{
    
    static func getLocationForUser(id: String, completion: @escaping (Bool, [Location]?) -> Void) {
        let url = "locations/\(id)"
        getRequestArray(url: url, completion: {(result: [JSON]?) in
            var locations: [Location] = []
            if result != nil {
                
                for json: JSON in result! {
                    if let medication = Location(json: json) {
                        locations.append(medication)
                    }
                }
                completion(true, locations)
            } else {
                completion(false, nil)
            }
        })
    }

    static func addLocation(parameters : [String: AnyObject]? = nil, completion: @escaping (Bool) -> Void) {
        let url = "locations"
        postRequest(url: url, parameters: parameters, completion: {(result: JSON?) in
            completion(true)
        })
    }

}
