//
//  TextCell.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 14/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class TextCell : UITableViewCell {
    
    public var delegate : nameDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
 
    @IBAction func editingChanged(_ sender: Any) {
        let tf = sender as! UITextField
        if let text = tf.text {
            self.delegate?.nameDidChange(value: text)
        }
    }

}
