//
//  UIFont + Utils.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension UIFont {
    
    static func appFontDemiBold(size : CGFloat) -> UIFont{
        return UIFont(name: "AvenirNext-DemiBold", size: size)!
    }
    
}
