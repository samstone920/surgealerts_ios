//
//  SwitchCell.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 14/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class SwitchCell : UITableViewCell {
    
    public enum SwitchType : Int{
        case vibration
        case sounds 
    }
    
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var switchUI: UISwitch!
    
    private var delegate : switchDelegate?
    private var type : SwitchType?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }
    
    func refresh(labelTitle : String, type : SwitchType, delegate : switchDelegate) {
        title.text = labelTitle
        self.delegate = delegate
        self.type = type
    }
    
    @IBAction func switched(_ sender: Any) {
        let button = sender as! UISwitch
        if let t = self.type {
            self.delegate?.buttonDidChange(isOn: button.isOn, type: t)
        }
    }
    
}
