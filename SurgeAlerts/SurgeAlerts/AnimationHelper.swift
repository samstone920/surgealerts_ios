//
//  AnimationHelper.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class AnimationHelper {
    
    static func slide(duration: TimeInterval = 1.0, yDelta : CGFloat, xDelta : CGFloat, view : UIView) {
        UIView.animate(withDuration: duration, animations: {
            view.frame.origin.y -= yDelta
            view.frame.origin.x -= xDelta
        })
    }
    
    static func fadeIn(duration: TimeInterval = 1.0, view : UIView) {
        UIView.animate(withDuration: duration, animations: {
            view.alpha = 1.0
        })
    }
    
    static func fadeOut(duration: TimeInterval = 1.0, view : UIView) {
        UIView.animate(withDuration: duration, animations: {
            view.alpha = 0.0
        }, completion:{
            (value: Bool) in
            view.removeFromSuperview()
        })
    }
    
}
