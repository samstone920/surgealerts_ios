//
//  BaseService.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 16/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire

class BaseService{
   
    private static let baseURL = "http://192.168.1.66:8080/api/"

    static func getRequestArray(url: String, parameters: [String: AnyObject]? = nil, completion: @escaping ([JSON]?) -> Void) {
        self.requestArray(method: .get, url: url, parameters: parameters, completion:  completion)
    }
    
    static func getRequest(url: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON?) -> Void) {
        self.request(method: .get, url: url, parameters: parameters, completion:  completion)
    }
    
    static func postRequest(url: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON?) -> Void) {
        self.request(method: .post, url: url, parameters: parameters, completion:  completion)
    }
    
    static func putRequest(url: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON?) -> Void) {
        self.request(method: .put, url: url, parameters: parameters, completion:  completion)
    }
    
    static private func requestArray(method: Alamofire.HTTPMethod, url: String, parameters: [String: AnyObject]? = nil, completion: @escaping ([JSON]?) -> Void) {
        
        print("Trying to hit : " + baseURL + url + " with: \n \(parameters)")

        Alamofire.request(baseURL + url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                
                if let json = response.result.value as? [JSON] {
                    completion(json)
                    return
                } else{
                   print(response.response ?? "test")
                }
            
            
                completion(nil)
        }
    }
    
    static private func request(method: Alamofire.HTTPMethod, url: String, parameters: [String: AnyObject]? = nil, completion: @escaping (JSON?) -> Void) {
        
        print("Trying to hit : " + baseURL + url)
        Alamofire.request(baseURL + url, method: method, parameters: parameters, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
            
                print(response)
                
                if let json = response.result.value as? JSON {
                    completion(json)
                    return
                }
                
                completion(nil)
        }
    }
    
}
