//
//  String + Utils.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 19/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension String {

    func getDateFromJSONString() -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyy-MM-dd'T'HH:mm:ss.SZ"
        return dateFormatter.date(from: self)!
    }
}
