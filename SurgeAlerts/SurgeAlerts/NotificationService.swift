//
//  NotificationService.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 16/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss
import Alamofire
import UIKit

class NotificationService : BaseService{
    
    static func getNotificationsForUser(id: String, completion: @escaping (Bool, [Notification]?) -> Void) {
        let url = "notifications/\(id)"
        getRequestArray(url: url, completion: {(result: [JSON]?) in
            var notifications: [Notification] = []
            if result != nil {
                
                for json: JSON in result! {
                    if let notification = Notification(json: json) {
                        notifications.append(notification)
                    }
                }
                completion(true, notifications)
            } else {
                completion(false, nil)
            }
        })
    }
}
