//
//  Date + Utils.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 19/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

extension Date {
    
    func getNotificationDate()  -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, H:mm a"
        let timeStamp = dateFormatter.string(from: self)
        return timeStamp
    }
    
   
    
}
