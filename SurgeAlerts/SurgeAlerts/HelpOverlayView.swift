//
//  HelpOverlayView.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class HelpOverlayView : UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func setup(){
        self.backgroundColor = UIColor.gray.withAlphaComponent(0.8)
        
        let handImage = UIImageView(frame: CGRect(x : 0, y : self.frame.size.height/2, width: self.frame.size.width, height : self.frame.size.height/2))
        handImage.image = UIImage(named: "hand")
        handImage.contentMode = .scaleAspectFit
        handImage.alpha = 0.8
        
        let helpText = UILabel(frame: CGRect(x : 50, y: self.frame.size.height/2 - 100, width: self.frame.size.width-100, height : 100))
        helpText.text = "Press and hold to add a Surge Marker"
        helpText.font = .appFontDemiBold(size: 24)
        helpText.textAlignment = .center
        helpText.numberOfLines = 2
        helpText.textColor = .white
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(HelpOverlayView.hide))
        self.addGestureRecognizer(tap)

        self.addSubview(handImage)
        self.addSubview(helpText)
    }
    
    func hide(){
        AnimationHelper.fadeOut(duration: K.Animation.animationLength, view: self)
    }
    
    
}
