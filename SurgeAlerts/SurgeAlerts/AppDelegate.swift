//
//  AppDelegate.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import UIKit
import UberRides
import Instabug
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {

        Instabug.start(withToken: "3fb54b79d4d9fa9afe320884708cb4d0", invocationEvent: .none)
        OneSignal.initWithLaunchOptions(launchOptions, appId: "e5c99f0d-7824-46f1-966a-2e1c58b9b24b")

        return true
    }

    
    func application(_ app: UIApplication,
                     open url: URL,
                     options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        let sourceApplication: String? =
            options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
        return  RidesAppDelegate.sharedInstance.application(app, openURL: url, sourceApplication: sourceApplication, annotation: nil)
    }
    

}

