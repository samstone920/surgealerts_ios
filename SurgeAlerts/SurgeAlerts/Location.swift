//
//  Location.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 16/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import Gloss

//MARK: - RootClass
public struct Location: Glossy {
    
    
    public var lat : Double?
    public var longField : Double?
    public var name : String?
    public var sound : Bool?
    public var uberId : String?
    public var vibration : Bool?
    public var multiplyer : Float?

    public init(){
        
    }
    
    //MARK: Decodable
    public init?(json: JSON){
        lat = "lat" <~~ json
        longField = "long" <~~ json
        name = "name" <~~ json
        sound = "sound" <~~ json
        uberId = "uberId" <~~ json
        vibration = "vibration" <~~ json
        multiplyer = "multiplyer" <~~ json

    }
    
    
    //MARK: Encodable
    public func toJSON() -> JSON? {
        return jsonify([
            "lat" ~~> lat,
            "long" ~~> longField,
            "name" ~~> name,
            "sound" ~~> sound,
            "uberId" ~~> uberId,
            "vibration" ~~> vibration,
            "multiplyer" ~~> multiplyer,
            ])
    }
    
}
