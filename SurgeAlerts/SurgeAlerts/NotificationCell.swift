//
//  NotificationCell.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 19/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit

class NotificationCell : UITableViewCell {
    
    @IBOutlet weak var title: UILabel!

    @IBOutlet weak var notificaionImage: UIImageView!
    
    @IBOutlet weak var dateLabel: UILabel!


    public func refresh(notifcation : Notification) {
        guard let started = notifcation.started else {return}
        guard let multiplyer = notifcation.multiplyer else {return}
        guard let name = notifcation.name else {return}
        guard let date = notifcation.date else {return}
        guard let read = notifcation.read else {return}

        
        self.title.text = "A \(multiplyer)x Surge has \(started ? "started" : "ended") at \(name)"
        self.title.numberOfLines = 0
        self.title.lineBreakMode = NSLineBreakMode.byWordWrapping
        self.title.sizeToFit()
        self.notificaionImage.image = self.notificaionImage.image!.withRenderingMode(.alwaysTemplate)
        
        
        self.notificaionImage.tintColor = read ? UIColor.clear : UIColor.saOrange()
        self.dateLabel.text = date.getDateFromJSONString().getNotificationDate()

    }
}
