//
//  AddMarkerViewController.swift
//  SurgeAlerts
//
//  Created by Sam Stone on 13/12/2016.
//  Copyright © 2016 Sam Stone. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import Material

class AddMarkerViewController : UIViewController, UITableViewDataSource, UITableViewDelegate, sliderDelegate, switchDelegate, nameDelegate{
    
    private var location : CLLocation!
    
    @IBOutlet weak var tableView: UITableView!
    private var loc = Location.init()
    
    init(location : CLLocation) {
        super.init(nibName: "AddMarkerViewController", bundle: Bundle.main)
        self.location = location
        loc.lat = location.coordinate.latitude
        loc.longField = location.coordinate.longitude
        if let user = UserDefaults.getLoggedInUser(){
            loc.uberId = user.uberId
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Add Marker"
        self.tableView.register(UINib(nibName: "MapCell", bundle: nil), forCellReuseIdentifier: "MapCell")
        self.tableView.register(UINib(nibName: "TextCell", bundle: nil), forCellReuseIdentifier: "TextCell")
        self.tableView.register(UINib(nibName: "SliderCell", bundle: nil), forCellReuseIdentifier: "SliderCell")
        self.tableView.register(UINib(nibName: "SwitchCell", bundle: nil), forCellReuseIdentifier: "SwitchCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        setupFAB()
    }
    
    func setupFAB(){
        let tick = FabButton(image: Icon.cm.check , tintColor: .white)
        tick.pulseColor = .white
        tick.backgroundColor = Color.saOrange()
        
        view.layout(tick)
            .width(62)
            .height(62)
            .bottomRight(bottom: 24, right: 24)
        
        tick.addTarget(self, action: #selector(complete), for: .touchUpInside)
    }
    
    func complete(){
        LocationService.addLocation(parameters: loc.toJSON() as [String : AnyObject]?, completion:
        { (success) in
            if success{
                self.dismiss(animated: true, completion: nil)
            }
        })
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title : String
        switch section {
        case 0:
            title = "Location"
            break
        case 1:
            title = "Settings"
            break
        default:
            title = ""
            break
        }
        return title
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var sections : Int
        switch section{
        case 0:
            sections = 1
            break
        case 1:
            sections = 4
            break
        default:
            sections = 0
            break
        }
        
        return sections
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 && indexPath.section == 0 {
            let cell:MapCell = self.tableView.dequeueReusableCell(withIdentifier: "MapCell") as! MapCell
            cell.refresh(location: self.location)
            return cell
        } else if indexPath.row == 0 && indexPath.section == 1 {
            let cell:TextCell = self.tableView.dequeueReusableCell(withIdentifier: "TextCell") as! TextCell
            cell.delegate = self
            return cell
        } else if indexPath.row == 1 && indexPath.section == 1 {
            let cell:SliderCell = self.tableView.dequeueReusableCell(withIdentifier: "SliderCell") as! SliderCell
            cell.refresh(delegate: self)
            return cell
        } else if indexPath.row == 2 && indexPath.section == 1{
            let cell:SwitchCell = self.tableView.dequeueReusableCell(withIdentifier: "SwitchCell") as! SwitchCell
            cell.refresh(labelTitle: "Notification Sound", type: SwitchCell.SwitchType.sounds, delegate: self)
            return cell
        } else {
            let cell:SwitchCell = self.tableView.dequeueReusableCell(withIdentifier: "SwitchCell") as! SwitchCell
            cell.refresh(labelTitle: "Notification Vibration", type: SwitchCell.SwitchType.vibration, delegate : self)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height : CGFloat
        
        if indexPath.row == 0 && indexPath.section == 0 {
            height = 200
        } else if indexPath.row == 1 && indexPath.section == 1 {
            height = 150
        } else {
            height = 50
        }
        
        return height
    }

    func sliderValueDidChange(value: Float) {
        loc.multiplyer = value
    }
    
    func buttonDidChange(isOn: Bool, type: SwitchCell.SwitchType) {
        if type == SwitchCell.SwitchType.sounds {
            loc.sound = isOn
        } else {
            loc.vibration = isOn
        }
    }
    
    func nameDidChange(value: String) {
        loc.name = value
        print(value)
    }
    
}

protocol nameDelegate{
    func nameDidChange(value : String)
}

protocol sliderDelegate{
    func sliderValueDidChange(value : Float)
}

protocol switchDelegate{
    func buttonDidChange(isOn : Bool, type : SwitchCell.SwitchType)
}


